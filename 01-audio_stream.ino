
#include "AudioTools.h"
#include "AudioLibs/Communication.h"


// User config
const char *ssid = "TP-Link_1A44";
const char *password = "20436407";
IPAddress udpAddress(192, 168, 0, 12); 
const int udpPort = 12345;
AudioInfo info(16000, 1, 16);



I2SStream i2sStream; // Access I2S as stream
UDPStream udp(ssid, password); 
Throttle throttle(udp);
StreamCopy copier(throttle, i2sStream);  


void setup() {
    Serial.begin(115200);
    AudioLogger::instance().begin(Serial, AudioLogger::Info);

    // microphone set up
    auto cfg_i2s = i2sStream.defaultConfig(RX_MODE);
    cfg_i2s.copyFrom(info);
    cfg_i2s.i2s_format = I2S_STD_FORMAT;
    cfg_i2s.channel_format  = I2S_CHANNEL_FMT_ONLY_LEFT;  // try with left
    cfg_i2s.pin_bck = 26; // GPIO_NUM_26
    cfg_i2s.pin_ws = 32;
    cfg_i2s.pin_data = I2S_PIN_NO_CHANGE; // output
    cfg_i2s.pin_data_rx = 33; // input
    i2sStream.begin(cfg_i2s);

    // UDP Set up
    udp.begin(udpAddress, udpPort);
    // Define Throttle
    auto cfg = throttle.defaultConfig();
    cfg.copyFrom(info);
    //cfg.correction_ms = 0;
    throttle.begin(cfg);

    Serial.println("started...");
}

void loop() { 
    copier.copy();
}