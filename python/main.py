#%%
import socket

# Configura el servidor UDP para escuchar en el puerto 12345
host = '0.0.0.0'  # Escucha en todas las interfaces
port = 12345
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((host, port))

# Abre el archivo CSV para escribir los datos
with open('datos.csv', 'w') as file:
    while True:
        data, addr = s.recvfrom(1024)  # Recibe datos
        data_str = ','.join(str(byte) for byte in data)  # Convierte los bytes en una cadena CSV
        file.write(data_str + '\n')  # Escribe la cadena en el archivo CSV
        file.flush()  # Asegura que los datos se escriban inmediatamente en el archivo

#%%
"""
Read csv received from socket and convert to wav

"""

import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np

sample_rate = 16000

# Leer el archivo CSV
df = pd.read_csv('datos.csv', header=None)
data = df.values.flatten()

# Convertir pares de bytes en enteros de 16 bits con signo
# Combinar los bytes en enteros de 16 bits con signo
int_data = []
for i in range(0, len(data), 2):
    if i + 1 < len(data):
        byte1 = data[i]
        byte2 = data[i + 1]
        int_value = (byte2 << 8) | byte1
        if int_value > 32767:
            int_value -= 65536  # Convertir a un valor con signo de 16 bits
        int_data.append(int_value)

int_data = np.array(int_data)

# Graficar los datos
plt.plot(int_data)
plt.xlabel('Samples')
plt.ylabel('Amplitude')
plt.title('Audio Plot')
plt.show()

# Guardar los datos como un archivo de audio WAV
wavfile.write('audio.wav', sample_rate, int_data.astype(np.int16))

